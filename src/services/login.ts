import request, { loginRequest } from '@/utils/request';
import { param } from '@/utils/utils';


export type LoginParamsType = {
  username: string;
  password: string;
  mobile: string;
  captcha: string;
  grant_type: string;
};

export async function fakeAccountLogin(params: LoginParamsType) {
  params.grant_type = 'password'
  return loginRequest('/auth/oauth/token' + param(params), {
    method: 'POST'
  });
}

export async function getFakeCaptcha(mobile: string) {
  return request(`/api/login/captcha?mobile=${mobile}`);
}
