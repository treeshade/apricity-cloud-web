declare module 'slash2';
declare module '*.css';
declare module '*.less';
declare module '*.scss';
declare module '*.sass';
declare module '*.svg';
declare module '*.png';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.gif';
declare module '*.bmp';
declare module '*.tiff';
declare module 'omit.js';

// google analytics interface
type GAFieldsObject = {
  eventCategory: string;
  eventAction: string;
  eventLabel?: string;
  eventValue?: number;
  nonInteraction?: boolean;
};

interface Window {
  ga: (
    command: 'send',
    hitType: 'event' | 'pageview',
    fieldsObject: GAFieldsObject | string,
  ) => void;
  reloadAuthorized: () => void;
  Apricity: {
    download: (url: string, params?: any) => void,
    resolveExt$: (data: any[]) => any[]
  }
}

declare let ga: () => void;

// preview.pro.ant.design only do not use in your production ;
// preview.pro.ant.design 专用环境变量，请不要在你的项目中使用它。
declare let ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION: 'site' | undefined;

declare const REACT_APP_ENV: 'test' | 'dev' | 'pre' | false;

declare interface ParamProps {
  n?: number;//当前页
  s?:number;//每页数量
  f? : object;//筛选条件
  qf? : object;//快捷查询过滤条件的json
  o?: object;// 排序 "排序字段的名称" : "asc或desc"<br>
}

declare function getJsonWrapper(p: ParamProps, b:any[]): any;


declare function restPrint(s:string, o:any):string
