import request from '@/utils/request';


export async function roleInsert(data:any){
    return request.post('/auth/core/role', {data: getJsonWrapper({}, ['', [{...data}]])})
}

export async function roleInstant(service:string, data:any) {
    return request.put('/auth/core/role/instant', {data: getJsonWrapper({}, [service, [{...data}]])});
}


export async function roleDelete(data:any[]) {
  return request.delete('/auth/core/role', {data: getJsonWrapper({}, ['', data])});
}


export async function rolePermissionInsert(id:any, permissionTypeId:any, data:any[]){
  return request.post(
    restPrint('/auth/core/role/{id}/role-permission-types/{permissionTypeId}/role-permissions', {id: id, permissionTypeId: permissionTypeId}), 
    {data: getJsonWrapper({}, ['', [...data]])}
    )
}



export async function rolePermissionDelete(id:any, permissionTypeId:any, data:any[]){
  return request.delete(
    restPrint('/auth/core/role/{id}/role-permission-types/{permissionTypeId}/role-permissions', {id: id, permissionTypeId: permissionTypeId}), 
    {data: getJsonWrapper({}, ['', [...data]])}
    )
}


export async function roleUserInsert(id:any, data:any[]){
  return request.post(
    restPrint('/auth/core/role/{id}/role-user', {id: id}), 
    {data: getJsonWrapper({}, ['', [...data]])}
    )
}



export async function roleUserDelete(id:any, data:any[]){
  return request.delete(
    restPrint('/auth/core/role/{id}/role-user', {id: id}), 
    {data: getJsonWrapper({}, ['', [...data]])}
    )
}