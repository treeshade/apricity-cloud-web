import ApTable, { ApTableRef } from '@/components/Table';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Col, Input, InputNumber, Modal, Popconfirm, Row, Tabs } from 'antd';
import React, { useRef, useState } from 'react';
import Form, { FormInstance } from 'antd/lib/form';
import UserChoosePage from '../user/userChoosePage';
import { 
  roleInsert,
  roleDelete,
  rolePermissionInsert,
  rolePermissionDelete,
  roleUserInsert,
  roleUserDelete
} from './service';
import OrgTreeChoosePage from '../org/orgTreeChoose';

const { TabPane } = Tabs;

const coreRoleInsertFormRef = React.createRef<FormInstance>();

const coreRoleTableRef = React.createRef<ApTableRef>();

const roleOrgTableRef = React.createRef<ApTableRef>();

const roleUserTableRef = React.createRef<ApTableRef>();

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

const coreRoleColumns = [
  {
    title: '角色编码',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: '角色名称',
    dataIndex: 'roleName',
    key: 'roleName',
    editable: true,
  },
  {
    title: '排序码',
    dataIndex: 'orderNo',
    key: 'orderNo',
    editable: true,
  },
];

const coreOrgColumns = [
  {
    title: '部门编号',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: '部门名称',
    dataIndex: 'orgName',
    key: 'orgName',
  },
];


const userColumns = [
  {
      title: '用户名',
      dataIndex: 'username',
      key: 'username'
    },
    {
      title: '人员编码',
      dataIndex: 'userNo',
      key: 'userNo',
    },
];

const CoreRolePage: React.FC = () => {
  const roleSelectedRowsRef = useRef([] as any);

  const roleOrgSelectedRowsRef = useRef([] as any);

  const roleUserSelectedRowsRef = useRef([] as any);

  //hooks
  const [coreRoleInserModalVisible, setCoreRoleInserModalVisibleState] = useState(false);
  const [orgChooseState, setOrgChooseState] = useState(false);
  const [userChooseState, setUserChooseState] = useState(false);
  const currRoleRow = useRef({} as any)

  //function
  const handleCoreRoleCellChange = (field: any, oldVal: any, newVal: any) => {};

  const handleCoreRoleFormChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    field: string,
  ) => {};

  const handleCoreRoleSelectChange = (selectedRowKeys: any, selectedRows: any) => {
    roleSelectedRowsRef.current = selectedRows;
  };

  const handleRoleOrgSelectChange = (selectedRowKeys: any, selectedRows: any) => {
    roleOrgSelectedRowsRef.current = selectedRows;
  };

  const handleRoleUserSelectChange = (selectedRowKeys: any, selectedRows: any) => {
    roleUserSelectedRowsRef.current = selectedRows;
  };

  const handleCoreRoleCurrentRowChange = (row: any) => {
    currRoleRow.current = row
    roleUserTableRef.current?.refresh(
      '/auth/user/queries/raw',
      {
        'role_assign' : currRoleRow.current.id
      }
    )
  };

  const handleCoreRoleInsert = () => {
    setCoreRoleInserModalVisibleState(true);
  };

  const handleCoreRoleDelete = () => {
    roleDelete(roleSelectedRowsRef.current).then((res) => {
      coreRoleTableRef.current?.refresh();
    });
  };


  const handleRoleOrgDelete = () => {
    const permissions =  roleOrgSelectedRowsRef.current.map((row: { roleId: any; permissionTypeId: string; targetId: any; id: any; }) => {
      row.roleId = currRoleRow.current.id
      row.permissionTypeId = 'T_CORE_ORG'
      row.targetId = row.id
      return row
    })
    rolePermissionDelete(currRoleRow.current.id, 'T_CORE_ORG', permissions).then((res) => {
      roleOrgTableRefresh()
    });
  };

  const handleRoleUserDelete = () => {
    roleUserDelete(currRoleRow.current.id, roleUserSelectedRowsRef.current).then((res) => {
      roleUserTableRefresh()
    });
  };


  const roleOrgTableRefresh = ()=> {
    roleOrgTableRef.current?.refresh(
      '/auth/core/org/queries/raw',
      {
        'role_assign' : currRoleRow.current.id
      }
    )
  }


  const roleUserTableRefresh = ()=> {
    roleUserTableRef.current?.refresh(
      '/auth/user/queries/raw',
      {
        'role_assign' : currRoleRow.current.id
      }
    )
  }

  const handleOrgChooseClose = (rows: any) => {
    setOrgChooseState(false)
    if(rows){
      const permissions =  rows.map((row: { roleId: any; permissionTypeId: string; targetId: any; id: any; }) => {
        row.roleId = currRoleRow.current.id
        row.permissionTypeId = 'T_CORE_ORG'
        row.targetId = row.id
        return row
      })

      rolePermissionInsert(currRoleRow.current.id, 'T_CORE_ORG', permissions).then(res => {
        roleOrgTableRefresh()
      })
    }
  }


  const handleUserChooseClose = (rows: any) => {
    setUserChooseState(false)
    if(rows){
      const userList =  rows.map((row: { roleId: any; userId: string; id: any; }) => {
        row.roleId = currRoleRow.current.id
        row.userId = row.id
        return row
      })

      roleUserInsert(currRoleRow.current.id, userList).then(res => {
        roleUserTableRefresh()
      })
    }
  }

  const handleTabChange = (activeKey: any) => {
    switch (activeKey) {
      case '2':
        roleOrgTableRefresh()
        break;
    
      default:
        break;
    }
  }

  return (
    <>
      <PageContainer>
        <Row style={{ backgroundColor: '#ffffff' }} gutter={16}>
          <Col span={12}>
            <ApTable
              ref={coreRoleTableRef}
              size="small"
              hightCurrentRow
              onCellChange={handleCoreRoleCellChange} //单元格值改变
              rowSelection={{
                type: 'checkbox',
                onChange: handleCoreRoleSelectChange,
              }}
              loadSuccess={(rows) => {
                if (rows && rows.length > 0) {
                  coreRoleTableRef.current?.activeRowByIndex(0); //设置当前行
                }
              }}
              url={'/auth/core/role/queries/raw'}
              currentRowChange={handleCoreRoleCurrentRowChange}
              columns={coreRoleColumns}
              toolbarRender={() => (
                <>
                  <Button type={'primary'} onClick={handleCoreRoleInsert}>
                    新建
                  </Button>

                  <Popconfirm
                    title="您确认要删除吗?"
                    okText="确定"
                    cancelText="取消"
                    onConfirm={handleCoreRoleDelete}
                  >
                    <Button type="primary" danger>
                      删除
                    </Button>
                  </Popconfirm>
                </>
              )}
            />
          </Col>
          <Col span={12}>
            <Tabs onChange={ handleTabChange } defaultActiveKey="1">
              <TabPane tab="菜单权限" key="1"></TabPane>
              <TabPane tab="单位权限" key="2">
                <ApTable
                  ref={roleOrgTableRef}
                  size="small"
                  rowSelection={{
                    type: 'checkbox',
                    onChange: handleRoleOrgSelectChange,
                  }}
                  url = {'/auth/core/org/queries/raw'}
                  requestData = {
                    {
                      role_assign: currRoleRow.current.id
                    }
                  }
                  columns={coreOrgColumns}
                  toolbarRender={() => (
                    <>
                      <Button type={'primary'} onClick={() => setOrgChooseState(true)}>
                        添加
                      </Button>

                      <Popconfirm
                        title="您确认要删除吗?"
                        okText="确定"
                        cancelText="取消"
                        onConfirm={handleRoleOrgDelete}
                      >
                        <Button type="primary" danger>
                          删除
                        </Button>
                      </Popconfirm>
                    </>
                  )}
                />
              </TabPane>
            </Tabs>
            <Tabs defaultActiveKey="1">
              <TabPane tab="所属用户" key="1">
              <ApTable
                  ref={roleUserTableRef}
                  size="small"
                  rowSelection={{
                    type: 'checkbox',
                    onChange: handleRoleUserSelectChange,
                  }}
                  url = {'/auth/user/queries/raw'}
                  requestData = {
                    {
                      role_assign: currRoleRow.current.id
                    }
                  }
                  columns={userColumns}
                  toolbarRender={() => (
                    <>
                      <Button type={'primary'} onClick={() => setUserChooseState(true)}>
                        添加
                      </Button>

                      <Popconfirm
                        title="您确认要删除吗?"
                        okText="确定"
                        cancelText="取消"
                        onConfirm={handleRoleUserDelete}
                      >
                        <Button type="primary" danger>
                          删除
                        </Button>
                      </Popconfirm>
                    </>
                  )}
                />
              </TabPane>
            </Tabs>
          </Col>
        </Row>

        <Modal
          title="新增"
          centered
          visible={coreRoleInserModalVisible}
          onOk={() => {
            const formData = coreRoleInsertFormRef.current?.getFieldsValue();
            setCoreRoleInserModalVisibleState(false);
            roleInsert(formData).then((res) => {
              coreRoleTableRef.current?.refresh();
            });
          }}
          onCancel={() => setCoreRoleInserModalVisibleState(false)}
        >
          <Form {...formItemLayout} ref={coreRoleInsertFormRef}>
            <Form.Item
              label="角色编码"
              name="id"
              rules={[{ required: true, message: 'please input' }]}
            >
              <InputNumber />
            </Form.Item>

            <Form.Item
              label="角色名称"
              name="roleName"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreRoleFormChange(event, 'roleName')} />
            </Form.Item>
            <Form.Item label="角色说明" name="roleDesc">
              <Input onChange={(event) => handleCoreRoleFormChange(event, 'roleDesc')} />
            </Form.Item>
            <Form.Item label="排序码" name="orderNo">
              <Input onChange={(event) => handleCoreRoleFormChange(event, 'orderNo')} />
            </Form.Item>
          </Form>
        </Modal>
        <OrgTreeChoosePage onClose={ handleOrgChooseClose } visible={orgChooseState} />

        <UserChoosePage onClose= { handleUserChooseClose } visible={userChooseState}/>
      </PageContainer>
    </>
  );
};
export default CoreRolePage;
