import Apricity from '@/utils/apricity';
import { Col, Drawer, Form, Input, Row, Select, Tabs } from 'antd';
import { DrawerProps } from 'antd/lib/drawer';
import React, { useEffect, useState } from 'react';
import { instant } from './service';

const { TabPane } = Tabs;

const { Option } = Select;

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

interface UserDetailDrawerProps {
  form?: any;
  user?: any;
}
const UserDetailDrawer: React.FC<DrawerProps & UserDetailDrawerProps> = ({
  form,
  user,
  ...props
}) => {
  const handleCoreUserFormChange = (event: React.ChangeEvent<HTMLInputElement> | string, field: string) => {
    instant('coreUserServiceImpl', {
      id: user?.id,
      [field]: event.target.value,
    });
  };

  const handleCoreUserStatusChange = (value:any) => {
    instant('coreUserServiceImpl', {
      id: user?.id,
      status: value,
    });
  }

  const [userStatusState, setUserStatusState] = useState([] as any[]);

  useEffect(() => {
    if (props.visible) {
      Apricity.get('/auth/sys/code/queries/selectable' + Apricity.param({codeCategoryId_EQ: 'userStatus'})).then((res) => {
        if (res) {
          const items = (res as any[]).map((item) => {
            return { text: item['codeName'], val: item['code'] };
          });
          setUserStatusState(items);
        }
      });
    }
  }, [props.visible]);

  return (
    <Drawer {...props} placement="right" width={'calc(100% - 208px)'} getContainer={false}>
      <Tabs defaultActiveKey="1">
        <TabPane tab="基本信息" key="1">
          <Form {...formItemLayout} ref={form}>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  label="用户名"
                  name="username"
                  rules={[{ required: true, message: 'please input' }]}
                >
                  <Input onChange={(event) => handleCoreUserFormChange(event, 'username')} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="人员编码" name="userNo">
                  <Input onChange={(event) => handleCoreUserFormChange(event, 'userNo')} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="手机" name="mobile">
                  <Input onChange={(event) => handleCoreUserFormChange(event, 'mobile')} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="邮箱" name="email">
                  <Input onChange={(event) => handleCoreUserFormChange(event, 'email')} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="身份证" name="idCard">
                  <Input onChange={(event) => handleCoreUserFormChange(event, 'idCard')} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="用户状态" name="status">
                  <Select  onChange={ (value) =>  handleCoreUserStatusChange(value) }>
                    {userStatusState.map((userStatus) => {
                      return (
                        <Option key={userStatus.val} value={userStatus.val}>
                          {userStatus.text}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="单位编码" name="orgId">
                  <Input onChange={(event) => handleCoreUserFormChange(event, 'orgId')} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="单位名称" name="orgName">
                  <Input onChange={(event) => handleCoreUserFormChange(event, 'orgName')} />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </TabPane>
      </Tabs>
      <Tabs defaultActiveKey="1">
        <TabPane tab="访问部门" key="1"></TabPane>
        <TabPane tab="访问角色" key="2">
          Content of Tab Pane 1
        </TabPane>
      </Tabs>
    </Drawer>
  );
};

export default UserDetailDrawer;
