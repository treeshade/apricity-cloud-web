import ApTable, { ApTableRef } from '@/components/Table';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Input, Modal, Popconfirm } from 'antd';
import React, {  useState } from 'react';
import Form, { FormInstance } from 'antd/lib/form';
import UserDetailDrawer from './DetailDrawer';

import { 
  queryDetailById,
  insertUser,
  deleteUser
} from './service'

const coreUserTableRef = React.createRef<ApTableRef>();
const coreUserInsertFormRef = React.createRef<FormInstance>();
const coreUserEditFormRef = React.createRef<FormInstance>();


const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

const CoreUserPage: React.FC = () => {
  const [drawerVisible, setDrawerVisible] = useState(false);
  const [coreUserInserModalVisible, setCoreUserInserModalVisibleState] = useState(false);
  const [userState, setUserState] = useState({} as any);
  

  const coreUserColumns = [
    {
      title: '用户名',
      dataIndex: 'username',
      key: 'username',
      render: (text: string) => (
        <a
          onClick={() => {
            setDrawerVisible(!drawerVisible);
            const currentRow = coreUserTableRef.current?.getCurrebtRow()
            queryDetailById(currentRow.id).then(res => {
              coreUserEditFormRef.current?.setFieldsValue({...res})
              setUserState({...res})
            })
          }}
        >
          {text}
        </a>
      ),
    },
    {
      title: '人员编码',
      dataIndex: 'userNo',
      key: 'userNo',
    },
    {
      title: '手机',
      dataIndex: 'mobile',
      key: 'mobile',
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: '身份证',
      dataIndex: 'idCard',
      key: 'idCard',
    },
    {
      title: '用户状态',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: '单位编码',
      dataIndex: 'orgId',
      key: 'orgId',
    },
    {
      title: '单位名称',
      dataIndex: 'orgName',
      key: 'orgName',
    },
  ];

  //hooks
  const [userSlelectedRowsState, setUserSlelectedRowsState] = useState([] as any[])

  //function
  const handleCoreUserCellChange = (field: any, oldVal: any, newVal: any) => {};

  const handleCoreUserSelectChange = (selectedRowKeys: any, selectedRows: any) => {
    setUserSlelectedRowsState(selectedRows)
  };

  const handleCoreUserCurrentRowChange = (row: any) => {};

  const handleCoreUserInsert = () => {
    setCoreUserInserModalVisibleState(true);
  };

  const handleCoreUserDelete = () => {
    deleteUser(userSlelectedRowsState).then(res => {
      coreUserTableRef.current?.refresh()
    })
  };

  return (
    <>
      <PageContainer>
        <ApTable
          ref={coreUserTableRef}
          size="small"
          hightCurrentRow
          onCellChange={handleCoreUserCellChange} //单元格值改变
          rowSelection={{
            type: 'checkbox',
            onChange: handleCoreUserSelectChange,
          }}
          loadSuccess={(rows) => {
            if (rows && rows.length > 0) {
              coreUserTableRef.current?.activeRowByIndex(0); //设置当前行
            }
          }}
          url={'/auth/user/queries/raw'}
          currentRowChange={handleCoreUserCurrentRowChange}
          columns={coreUserColumns}
          toolbarRender={() => (
            <>
              <Button type={'primary'} onClick={handleCoreUserInsert}>
                新建
              </Button>

              <Popconfirm
                title="您确认要删除吗?"
                okText="确定"
                cancelText="取消"
                onConfirm={handleCoreUserDelete}
              >
                <Button type="primary" danger>
                  删除
                </Button>
              </Popconfirm>
            </>
          )}
        />
        <Modal
          title="新增"
          centered
          width={700}
          visible={coreUserInserModalVisible}
          onOk={() => {
            const formData = coreUserInsertFormRef.current?.getFieldsValue();
            insertUser(formData).then(res => {
              coreUserTableRef.current?.refresh()
            })
            coreUserTableRef.current?.refresh();
            setCoreUserInserModalVisibleState(false);
          }}
          onCancel={() => setCoreUserInserModalVisibleState(false)}
        >
          <Form {...formItemLayout} ref={coreUserInsertFormRef}>
            <Form.Item
              label="用户名"
              name="username"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="人员编码"
              name="userNo"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input/>
            </Form.Item>
            {/* <Form.Item
              label="用户密码"
              name="password"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input/>
            </Form.Item> */}
            <Form.Item
              label="手机"
              name="mobile"
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="邮箱"
              name="email"
            >
              <Input/>
            </Form.Item>
            {/* <Form.Item
              label="身份证"
              name="idCard"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input/>
            </Form.Item>

            <Form.Item
              label="用户状态"
              name="status"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input/>
            </Form.Item>
            <Form.Item
              label="单位编码"
              name="orgId"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="单位名称"
              name="orgName"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input/>
            </Form.Item> */}
          </Form>
        </Modal>
        <UserDetailDrawer
          onClose={() => setDrawerVisible(false)}
          visible={drawerVisible}
          form={coreUserEditFormRef}
          user={userState}
        ></UserDetailDrawer>
      </PageContainer>
    </>
  );
};

export default CoreUserPage;
