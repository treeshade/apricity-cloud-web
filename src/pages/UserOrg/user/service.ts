import request from '@/utils/request';

export async function queryDetailById(id: any) {
  return request.get(restPrint('/auth/user/{id}', {id: id}));
}

export async function instant(service:string, param:any) {
  return request.put('/auth/user/instant', {data: getJsonWrapper({}, [service, [{...param}]])});
}

export async function insertUser(data:any) {
  return request.post('/auth/user', {data: getJsonWrapper({}, ['', [data]])});
}


export async function deleteUser(data:any[]) {
  return request.delete('/auth/user', {data: getJsonWrapper({}, ['', data])});
}


