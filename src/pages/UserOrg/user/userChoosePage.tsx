import {Modal} from "antd"
import ApTable, { ApTableRef } from '@/components/Table';
import React, { useEffect, useRef, useState } from 'react';

const userChooseTableRef = React.createRef<ApTableRef>();

interface UserChoosePageProps {
    visible?:boolean;
    width?: number;
    onClose?: (rows?: any) => void;
}

const userChooseColumns = [
    {
        title: '用户名',
        dataIndex: 'username',
        key: 'username'
      },
      {
        title: '人员编码',
        dataIndex: 'userNo',
        key: 'userNo',
      },
  ];

const UserChoosePage:React.FC<UserChoosePageProps> = ({
    visible,
    width,
    onClose
}) => {

    const userSelectedRowsRef = useRef([] as any[]);
    const [visibleState, setVisibleState] = useState(visible);
    
    useEffect(() => {
        if(visible){
            setVisibleState(true);
        }
    })

    const [selectedKeysState, setSelectedKeysState] = useState([] as any[])

    const handleSelectChange = (selectedRowKeys: any, selectedRows: any) => {
        setSelectedKeysState(selectedRowKeys)
        userSelectedRowsRef.current = selectedRows
    }

    return (
        <Modal 
        onCancel={() => {
            setVisibleState(false);
            setSelectedKeysState([] as any[])
            onClose?.();
          }}
          onOk={() => {
            setVisibleState(false);
            setSelectedKeysState([] as any[])
            onClose?.(userSelectedRowsRef.current);
          }}
          visible={visibleState}
          width={width ? width : 900}
        >
            <ApTable
                url={'/auth/user/queries/raw'}
                requestData = {{
                    role_not_assign: '1'
                }}
                ref={userChooseTableRef}
                columns= {userChooseColumns}
                rowSelection={{
                    type: 'checkbox',
                    onChange: handleSelectChange,
                    selectedRowKeys: selectedKeysState
                  }}
            />
        </Modal>
    )
}

export default UserChoosePage