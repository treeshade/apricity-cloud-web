import ApTable, { ApTableRef } from '@/components/Table';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Card, Col, Form, Input, Modal, Popconfirm, Row, Tree } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { FormInstance } from 'antd/lib/form';
import { 
  selectOrgTree,
  orgInsert, 
  orgInstant,
  orgDelete
 } from './service';

const coreOrgTableRef = React.createRef<ApTableRef>();

const coreOrgInsertFormRef = React.createRef<FormInstance>();

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

const coreOrgColumns = [
  {
    title: '部门编号',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: '部门名称',
    dataIndex: 'orgName',
    key: 'orgName',
    editable: true,
  },
];

const CoreOrgPage: React.FC = () => {
  //hooks
  const [coreOrgInserModalVisible, setCoreOrgInserModalVisibleState] = useState(false);
  const [orgTreeDataState, setOrgTreeDataSatate] = useState([] as any);
  const orgTreeSelectedNodeRef = useRef({} as any);
  const orgTreeSelectedStatusRef = useRef(false);
  const orgSelectedRowsRef =  useRef([] as any[])

  useEffect(() => {
    refreshOrgTreeData()
  }, [])
  useEffect(() => {
    //清空form
    if (coreOrgInserModalVisible) {
      coreOrgInsertFormRef.current?.resetFields();
    }
  }, [coreOrgInserModalVisible]);

  const refreshOrgTreeData = () => {
    selectOrgTree().then((res) => {
      setOrgTreeDataSatate(res);
    });
  }

  //function
  const handleCoreOrgCellChange = (field: any, oldVal: any, newVal: any) => {
    orgInstant('coreOrgServiceImpl', { id: oldVal.id, [field]: newVal[field] });
  };

  const handleCoreOrgFormChange = (event: React.ChangeEvent<HTMLInputElement>, field: string) => {};

  const handleCoreOrgSelectChange = (selectedRowKeys: any, selectedRows: any) => {
    orgSelectedRowsRef.current = selectedRows
  };

  const handleCoreOrgCurrentRowChange = (row: any) => {};

  const handleCoreOrgInsert = () => {
    setCoreOrgInserModalVisibleState(true);
    coreOrgInsertFormRef.current?.setFieldsValue({parentTitle:  orgTreeSelectedNodeRef.current?.title })
  };

  const handleCoreOrgDelete = () => {
    orgDelete(orgSelectedRowsRef.current).then(res => {
      refreshOrgTreeData()
      coreOrgTableRef.current?.refresh()
    })
  };


  const handleOrgTreeSelect = (selectKeys: any, info: any) => {
    if(info.selected){
      orgTreeSelectedNodeRef.current= info.node
    }else{
      orgTreeSelectedNodeRef.current = ({} as any)
    }
    orgTreeSelectedStatusRef.current = info.selected
    coreOrgTableRef.current?.refresh(
      '/auth/core/org/queries/raw',
      {
        parentId_EQ: orgTreeSelectedNodeRef.current.id ? orgTreeSelectedNodeRef.current.id : 'VOID'
      }
    )
  }

  return (
    <>
      <PageContainer>
        <Row style={{ backgroundColor: '#ffffff' }} gutter={16}>
          <Col span={6}>
            <Card bordered={false} size="small" title="组织机构">
              <Tree onSelect={ handleOrgTreeSelect } treeData={ orgTreeDataState }>
              </Tree>
            </Card>
          </Col>
          <Col span={18}>
            <ApTable
              ref={coreOrgTableRef}
              size="small"
              hightCurrentRow
              onCellChange={handleCoreOrgCellChange} //单元格值改变
              rowSelection={{
                type: 'checkbox',
                onChange: handleCoreOrgSelectChange,
              }}
              loadSuccess={(rows) => {
                if (rows && rows.length > 0) {
                  coreOrgTableRef.current?.activeRowByIndex(0); //设置当前行
                }
              }}
              requestData={ {
                parentId_EQ: orgTreeSelectedNodeRef.current.id ? orgTreeSelectedNodeRef.current.id : 'VOID'
              } }
              url={'/auth/core/org/queries/raw'}
              currentRowChange={handleCoreOrgCurrentRowChange}
              columns={coreOrgColumns}
              toolbarRender={() => (
                <>
                  <Button type={'primary'} onClick={handleCoreOrgInsert}>
                    新建
                  </Button>

                  <Popconfirm
                    title="您确认要删除吗?"
                    okText="确定"
                    cancelText="取消"
                    onConfirm={handleCoreOrgDelete}
                  >
                    <Button type="primary" danger>
                      删除
                    </Button>
                  </Popconfirm>
                </>
              )}
            />
          </Col>
        </Row>

        <Modal
          title="新增"
          centered
          visible={coreOrgInserModalVisible}
          onOk={() => {
            const formData = coreOrgInsertFormRef.current?.getFieldsValue();
            setCoreOrgInserModalVisibleState(false);
            if(orgTreeSelectedStatusRef.current) {
              formData['parentId'] = orgTreeSelectedNodeRef.current.id
            }
            
            orgInsert(formData).then((res) => {
              coreOrgTableRef.current?.refresh();
              refreshOrgTreeData()
            });
          }}
          onCancel={() => setCoreOrgInserModalVisibleState(false)}
        >
          <Form {...formItemLayout} ref={coreOrgInsertFormRef} initialValues={{parentTitle: '组织机构'}}>
            <Form.Item
              label="组织编码"
              name="id"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'id')} />
            </Form.Item>
            <Form.Item
              label="组织名称"
              name="orgName"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'orgName')} />
            </Form.Item>
            <Form.Item
              label="父组织"
              name="parentTitle"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input disabled onChange={(event) => handleCoreOrgFormChange(event, 'parentId')} />
            </Form.Item>
            {/* <Form.Item
              label="父组织编码"
              name="parentId"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'parentId')} />
            </Form.Item>
            <Form.Item
              label="组织类型<下拉列表，系统编码orgcategory>"
              name="orgCategory"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'orgCategory')} />
            </Form.Item>
            <Form.Item
              label="业务代码 A-Z"
              name="bizCode"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'bizCode')} />
            </Form.Item>
            <Form.Item
              label="部门属性"
              name="orgProperty"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'orgProperty')} />
            </Form.Item>
            <Form.Item
              label="排序码"
              name="orderNo"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'orderNo')} />
            </Form.Item>
            <Form.Item
              label="创建人编码"
              name="createdById"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'createdById')} />
            </Form.Item>
            <Form.Item
              label="创建人名称"
              name="createdByName"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'createdByName')} />
            </Form.Item>
            <Form.Item
              label="创建时间"
              name="createdTime"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'createdTime')} />
            </Form.Item>
            <Form.Item
              label="创建人单位编码"
              name="createdByOrgId"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'createdByOrgId')} />
            </Form.Item>
            <Form.Item
              label="创建人单位名称"
              name="createdByOrgName"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreOrgFormChange(event, 'createdByOrgName')} />
            </Form.Item> */}
          </Form>
        </Modal>
      </PageContainer>
    </>
  );
};
export default CoreOrgPage;
