import ApTable, { ApTableRef } from '@/components/Table';
import { Col, Modal, Row, Tree } from 'antd';
import React, { useEffect, useRef, useState } from 'react';

const coreOrgChooseTableRef = React.createRef<ApTableRef>();

import { selectOrgTree } from './service';

interface OrgTreeChoosePage {
  visible: boolean;
  width?: number;
  onClose?: (rows?: any) => void;
}

const coreOrgColumns = [
  {
    title: '部门编号',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: '部门名称',
    dataIndex: 'orgName',
    key: 'orgName',
  },
];

const OrgTreeChoosePage: React.FC<OrgTreeChoosePage> = ({ visible, width, onClose }) => {
  const [visibleState, setVisibleState] = useState(visible);
  const [orgSelectKeysState, setOrgSelectKeysState] = useState([] as any[]);
  const [orgTreeDataState, setOrgTreeDataSatate] = useState([] as any);
  const orgTreeSelectedNodeRef = useRef({} as any);
  const orgTreeSelectedStatusRef = useRef(false);
  const orgSelectedRowsRef = useRef([] as any[]);

  useEffect(() => {
    if (visible) {
      setVisibleState(true);
      selectOrgTree().then((res) => {
        setOrgTreeDataSatate(res);
      });
    }
  }, [visible]);

  const handleCoreOrgSelectChange = (selectedRowKeys: any, selectedRows: any) => {
    setOrgSelectKeysState(selectedRowKeys)
    orgSelectedRowsRef.current = selectedRows;
  };

  const handleOrgTreeSelect = (selectKeys: any, info: any) => {
    if (info.selected) {
      orgTreeSelectedNodeRef.current = info.node;
    } else {
      orgTreeSelectedNodeRef.current = {} as any;
    }
    orgTreeSelectedStatusRef.current = info.selected;
    coreOrgChooseTableRef.current?.refresh('/auth/core/org/queries/raw', {
      parentId_EQ: orgTreeSelectedNodeRef.current.id ? orgTreeSelectedNodeRef.current.id : 'VOID',
    });
  };

  return (
    <Modal
      onCancel={() => {
        setVisibleState(false);
        setOrgSelectKeysState([] as any[])
        onClose?.();
      }}
      onOk={() => {
        setVisibleState(false);
        setOrgSelectKeysState([] as any[])
        onClose?.(orgSelectedRowsRef.current);
      }}
      visible={visibleState}
      width={width ? width : 900}
    >
      <Row gutter={16}>
        <Col span={8}>
          <Tree onSelect={handleOrgTreeSelect} treeData={orgTreeDataState}></Tree>
        </Col>
        <Col span={16}>
          <ApTable
            ref={coreOrgChooseTableRef}
            size="small"
            hightCurrentRow
            rowSelection={{
              type: 'checkbox',
              onChange: handleCoreOrgSelectChange,
              selectedRowKeys: orgSelectKeysState
            }}
            loadSuccess={(rows) => {
              if (rows && rows.length > 0) {
                coreOrgChooseTableRef.current?.activeRowByIndex(0); //设置当前行
              }
            }}
            requestData={{
              parentId_EQ: orgTreeSelectedNodeRef.current.id
                ? orgTreeSelectedNodeRef.current.id
                : 'VOID',
            }}
            url={'/auth/core/org/queries/raw'}
            columns={coreOrgColumns}
          />
        </Col>
      </Row>
    </Modal>
  );
};

export default OrgTreeChoosePage;
