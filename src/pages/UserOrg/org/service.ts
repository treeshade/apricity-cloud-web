import request from '@/utils/request';

export async function selectOrgTree(parentId?: any) {
  const url = parentId ? restPrint('/auth/core/org/select/org-tree/{parentId}', {parentId: parentId}) : '/auth/core/org/select/org-tree'
  return request.get(url);
}

export async function orgInsert(data:any){
    return request.post('/auth/core/org', {data: getJsonWrapper({}, ['', [{...data}]])})
}

export async function orgInstant(service:string, data:any) {
    return request.put('/auth/core/org/instant', {data: getJsonWrapper({}, [service, [{...data}]])});
}


export async function orgDelete(data:any[]) {
  return request.delete('/auth/core/org', {data: getJsonWrapper({}, ['', data])});
}