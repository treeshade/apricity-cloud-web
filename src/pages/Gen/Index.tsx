import { Drawer, Form, Input, Row, Col, Tabs, Button, Popconfirm, Modal } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React from 'react';
import {
  queryRecordRaw,
  queryDetail,
  queryColumnRaw,
  instant,
  insertTable,
  deleteTable,
  insertColumn,
  deleteColumn,
  insertRecord,
  deleteRecord,
  genCodeService,
} from './service';
import ApTable, { ApTableRef } from '../../components/Table';
import { PageContainer } from '@ant-design/pro-layout';

const formRef = React.createRef<FormInstance>();

const recordInsertFormRef = React.createRef<FormInstance>();

const apTableRef = React.createRef<ApTableRef>();

const columnApTableRef = React.createRef<ApTableRef>();

const recordTableRef = React.createRef<ApTableRef>();

const { TabPane } = Tabs;

const workflowColumns = [
  {
    beNull: '1',
    comments: '工作流状态',
    length: 32,
    tableId: '',
    propertyName: 'processState',
    type: 'varchar',
  },
]

const pkColumns = [
  {
    beNull: '0',
    comments: '主键',
    length: 32,
    tableId: '',
    propertyName: 'id',
    type: 'bigint',
  }
]

const createColumns = [
  {
    beNull: '1',
    comments: '创建人编码',
    length: 32,
    tableId: '',
    propertyName: 'createdById',
    type: 'varchar',
  },
  {
    beNull: '1',
    comments: '创建人名称',
    length: 256,
    tableId: '',
    propertyName: 'createdByName',
    type: 'varchar',
  },
  {
    beNull: '1',
    comments: '创建时间',
    propertyName: 'createdTime',
    tableId: '',
    type: 'datetime',
  },
  {
    beNull: '1',
    comments: '创建人单位编码',
    tableId: '',
    length: 32,
    propertyName: 'createdByOrgId',
    type: 'varchar',
  },
  {
    beNull: '1',
    tableId: '',
    comments: '创建人单位名称',
    length: 256,
    propertyName: 'createdByOrgName',
    type: 'varchar',
  },
];

class Gen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      records: [],
      currentRecordRow: {},
      currentTableRow: {},
      currentTableRowId: '',
      tableRecords: [],
      columnRecords: [],
      visible: false,
      recordSelectionKeys: [],
      recordSelectionRows: [],
      tableSelectionKeys: [],
      tableSelectionRows: [],
      columnSelectionKeys: [],
      columnSelectionRows: [],
      insertRecordModalVisible: false,
      detailForm: {
        author: '',
        packageName: '',
        id: '',
        code: '',
      },
      columns: [
        {
          title: '编码',
          dataIndex: 'code',
          key: 'code',
          render: (text) => (
            <a
              onClick={() => {
                this.showDrawer(text);
              }}
            >
              {text}
            </a>
          ),
        },
        {
          title: '包名',
          dataIndex: 'packageName',
          key: 'packageName',
        },
        {
          title: '作者',
          dataIndex: 'author',
          key: 'author',
        },
        {
          title: '操作',
          dataIndex: 'oprate',
          key: 'oprate',
          render: (text, record, index) => (
            <Button
              type="primary"
              onClick={() => {
                this.genCode(record);
              }}
            >
              {'生成'}
            </Button>
          ),
        },
      ],
      tableColumns: [
        {
          title: '表名',
          dataIndex: 'tableName',
          key: 'tableName',
          editable: true,
        },
        {
          title: '注释',
          dataIndex: 'comments',
          key: 'comments',
          editable: true,
        },
      ],
      columnColumns: [
        {
          title: '属性',
          dataIndex: 'propertyName',
          key: 'propertyName',
          editable: true,
        },
        {
          title: '注释',
          dataIndex: 'comments',
          key: 'comments',
          editable: true,
        },
        {
          title: '类型',
          dataIndex: 'type',
          key: 'type',
          fieldType: 'select',
          category: 'code',
          editable: true,
          param: {
            codeCategoryId_EQ: 'databaseFieldType',
          },
        },
        {
          title: '长度',
          dataIndex: 'length',
          key: 'length',
          editable: true,
        },
        {
          title: '是否可为空',
          dataIndex: 'beNull',
          key: 'beNull',
          fieldType: 'select',
          category: 'code',
          param: {
            codeCategoryId_EQ: 'yesOrNo',
          },
          editable: true,
        },
      ],
    };
  }
  showDrawer = (code) => {
    const _this = this;
    const currentRow = this.state.records?.filter((record) => record.code === code)[0];
    this.setState({ currentRecordRow: currentRow });
    queryDetail(currentRow.id).then((res) => {
      _this.setState({ visible: true });
      formRef?.current?.setFieldsValue({ ...res });
    });
  };
  componentDidMount() {
    queryRecordRaw().then((res) => {
      const records = res.records;
      this.setState({ records });
    });
  }
  onDrawerClose = () => {
    this.setState({ visible: false });
    queryRecordRaw().then((res) => {
      const records = res.records;
      this.setState({ records });
    });
  };
  handleCellClick(row, event) {
    this.setState({ currentTableRowId: row.id });
    queryColumnRaw(row.id).then((res) => {
      this.setState({ columnRecords: res.records });
    });
  }
  setRowClassName(row, index) {
    return row.id === this.state.currentTableRowId ? `ant_table_row_active` : ``;
  }

  handleTableSelectChange(selectedRowKeys: any, selectedRows: any) {
    this.setState({ tableSelectionKeys: selectedRowKeys, tableSelectionRows: selectedRows });
  }

  handleColumnSelectChange(selectedRowKeys: any, selectedRows: any) {
    this.setState({ columnSelectionKeys: selectedRowKeys, columnSelectionRows: selectedRows });
  }

  handleRecordSelectChange(selectedRowKeys: any, selectedRows: any) {
    this.setState({ recordSelectionKeys: selectedRowKeys, recordSelectionRows: selectedRows });
  }

  handleRecordFormChange(e, field: string) {
    instant('genRecordServiceImpl', {
      id: this.state.currentRecordRow.id,
      [field]: e.target.value,
    });
  }

  handleTableInsert() {
    insertTable(this.state.currentRecordRow.id).then((res) => {
      apTableRef.current?.refresh();
    });
  }

  handleTableDelete() {
    deleteTable(this.state.tableSelectionRows).then((res) => {
      apTableRef.current?.refresh();
    });
  }

  handleColumnInsert() {
    insertColumn(this.state.currentTableRow.id).then((res) => {
      columnApTableRef.current?.refresh(
        restPrint('/gen/record/{tableId}/column/queries/raw', {
          tableId: this.state.currentTableRow.id,
        }),
      );
    });
  }

  handleMultiColumnInsert(data?: any[]) {
    insertColumn(this.state.currentTableRow.id, data).then((res) => {
      columnApTableRef.current?.refresh(
        restPrint('/gen/record/{tableId}/column/queries/raw', {
          tableId: this.state.currentTableRow.id,
        }),
      );
    });
  }

  handleColumnDelete() {
    deleteColumn(this.state.columnSelectionRows).then((res) => {
      columnApTableRef.current?.refresh(
        restPrint('/gen/record/{tableId}/column/queries/raw', {
          tableId: this.state.currentTableRow.id,
        }),
      );
    });
  }

  handleTableCellChange(field: string | number, oldVal: { id: any }, newVal: { [x: string]: any }) {
    instant('genTableServiceImpl', { id: oldVal.id, [field]: newVal[field] });
  }

  handleColumnCellChange(
    field: string | number,
    oldVal: { id: any },
    newVal: { [x: string]: any },
  ) {
    instant('genColumnServiceImpl', { id: oldVal.id, [field]: newVal[field] });
  }

  genCode = (record: any) => {
    genCodeService(record.id).then((res) => {});
  };

  renderDrawer = () => {
    if (this.state.visible) {
      return (
        <Drawer
          title="详情"
          placement="right"
          getContainer={false}
          width={'calc(100% - 208px)'}
          visible={this.state.visible}
          onClose={this.onDrawerClose}
        >
          <Form ref={formRef}>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  label="作者"
                  name="author"
                  rules={[{ required: true, message: 'Please input your username!' }]}
                >
                  <Input onChange={(e) => this.handleRecordFormChange(e, 'author')} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  label="包名"
                  name="packageName"
                  rules={[{ required: true, message: 'Please input your username!' }]}
                >
                  <Input onChange={(e) => this.handleRecordFormChange(e, 'packageName')} />
                </Form.Item>
              </Col>
            </Row>
          </Form>
          <Tabs defaultActiveKey="1">
            <TabPane tab="表信息" key="1">
              <ApTable
                ref={apTableRef}
                size="small"
                hightCurrentRow
                onCellChange={this.handleTableCellChange.bind(this)} //单元格值改变
                rowSelection={{
                  type: 'checkbox',
                  onChange: this.handleTableSelectChange.bind(this),
                }}
                loadSuccess={(rows) => {
                  //数据加载成功事件
                  if (rows && rows.length > 0) {
                    apTableRef.current?.activeRowByIndex(0); //设置当前行
                  }
                }}
                url={restPrint('/gen/record/{id}/table/queries/raw', {
                  //表格数据查询url
                  id: this.state.currentRecordRow.id,
                })}
                currentRowChange={(
                  row, //当前行change事件
                ) => {
                  this.setState({ currentTableRow: row, currentTableRowId: row.id });
                  columnApTableRef.current?.refresh(
                    //刷新副表数据
                    restPrint('/gen/record/{tableId}/column/queries/raw', { tableId: row.id }),
                  );
                }}
                columns={this.state.tableColumns}
                toolbarRender={() => (
                  <>
                    <Button type={'primary'} onClick={this.handleTableInsert.bind(this)}>
                      新建
                    </Button>

                    <Popconfirm
                      title="您确认要删除吗?"
                      okText="确定"
                      cancelText="取消"
                      onConfirm={this.handleTableDelete.bind(this)}
                    >
                      <Button type="primary" danger>
                        删除
                      </Button>
                    </Popconfirm>
                  </>
                )}
              />
            </TabPane>
          </Tabs>
          <Tabs defaultActiveKey="1">
            <TabPane tab="列信息" key="1">
              <ApTable
                onCellChange={this.handleColumnCellChange.bind(this)}
                size="small"
                rowSelection={{
                  type: 'checkbox',
                  onChange: this.handleColumnSelectChange.bind(this),
                }}
                ref={columnApTableRef}
                toolbarRender={() => (
                  <>
                    <Button type={'primary'} onClick={this.handleColumnInsert.bind(this)}>
                      新建
                    </Button>

                    <Popconfirm
                      title="您确认要删除吗?"
                      okText="确定"
                      cancelText="取消"
                      onConfirm={this.handleColumnDelete.bind(this)}
                    >
                      <Button type="primary" danger>
                        删除
                      </Button>
                    </Popconfirm>

                    <Button
                      onClick={this.handleMultiColumnInsert.bind(
                        this,
                        createColumns.map((col) => {
                          col.tableId = this.state.currentTableRow.id;
                          return col;
                        }),
                      )}
                    >
                      制单列
                    </Button>

                    <Button
                      onClick={this.handleMultiColumnInsert.bind(
                        this,
                        pkColumns.map((col) => {
                          col.tableId = this.state.currentTableRow.id;
                          return col;
                        }),
                      )}
                    >
                      主键
                    </Button>


                    <Button
                      onClick={this.handleMultiColumnInsert.bind(
                        this,
                        workflowColumns.map((col) => {
                          col.tableId = this.state.currentTableRow.id;
                          return col;
                        }),
                      )}
                    >
                      工作流
                    </Button>
                  </>
                )}
                columns={this.state.columnColumns}
              />
            </TabPane>
          </Tabs>
        </Drawer>
      );
    }
  };

  render() {
    return (
      <>
        <PageContainer>
          {this.renderDrawer()}
          <ApTable
            ref={recordTableRef}
            columns={this.state.columns}
            url={'/gen/record/queries/raw'}
            size="small"
            rowSelection={{
              type: 'checkbox',
              onChange: this.handleRecordSelectChange.bind(this),
            }}
            toolbarRender={() => (
              <>
                <Button
                  type={'primary'}
                  onClick={() => {
                    this.setState({ insertRecordModalVisible: true });
                  }}
                >
                  新建
                </Button>

                <Popconfirm
                  title="您确认要删除吗?"
                  okText="确定"
                  cancelText="取消"
                  onConfirm={() => {
                    deleteRecord(this.state.recordSelectionRows).then((res) => {
                      recordTableRef.current?.refresh();
                    });
                  }}
                >
                  <Button type="primary" danger>
                    删除
                  </Button>
                </Popconfirm>
              </>
            )}
          />

          <Modal
            title="新增"
            centered
            visible={this.state.insertRecordModalVisible}
            onOk={() => {
              const formData = recordInsertFormRef.current?.getFieldsValue();
              insertRecord(formData).then((res) => {
                recordTableRef.current?.refresh();
              });
              this.setState({ insertRecordModalVisible: false });
            }}
            onCancel={() => this.setState({ insertRecordModalVisible: false })}
          >
            <Form ref={recordInsertFormRef}>
              <Form.Item
                label="包名"
                name="packageName"
                rules={[{ required: true, message: '请填写' }]}
              >
                <Input />
              </Form.Item>
              <Form.Item label="作者" name="author" rules={[{ required: true, message: '请填写' }]}>
                <Input />
              </Form.Item>
            </Form>
          </Modal>
        </PageContainer>
      </>
    );
  }
}

export default Gen;
