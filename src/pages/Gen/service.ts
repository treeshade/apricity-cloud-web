import Apricity from '@/utils/apricity';
import request from '@/utils/request';

/*===========   record   ============*/
export async function insertRecord(data:any) {
  return request.post('/gen/record', {data: getJsonWrapper({}, ['', [{...data}]])});
}

export async function deleteRecord(data:any[]) {
  return request.delete('/gen/record', {data: getJsonWrapper({}, ['', data])});
}

export async function genCodeService(id:any[]) {
  Apricity.download(restPrint('/gen/record/{id}/action/gen', {id: id}))
}

export async function queryRecordRaw() {
  return request.post('/gen/record/queries/raw', {});
}

export async function queryDetail(id:number|string) {
  return request.get(restPrint('/gen/record/{id}', {id: id}));
}

/*========  table  ======*/
export async function queryTableRaw(id:number|string) {
  return request.post(restPrint('/gen/record/{id}/table/queries/raw', {id: id}));
}

export async function queryColumnRaw(tableId:number|string) {
  return request.post(restPrint('/gen/record/{tableId}/column/queries/raw', {tableId: tableId}));
}


export async function instant(service:string, param:any) {
  return request.put('/gen/record/instant', {data: getJsonWrapper({}, [service, [{...param}]])});
}

export async function insertTable(id:any) {
  return request.post(restPrint('/gen/record/{id}/table', {id: id}))
}

export async function deleteTable(data:any[]) {
  return request.delete('/gen/record/table', {data: getJsonWrapper({}, ['', data])})
}


export async function insertColumn(tableId:any, data?: any[]) {
  return request.post(restPrint('/gen/record/table/{tableId}/column', {tableId: tableId}), {data: getJsonWrapper({}, ['', data])})
}

export async function deleteColumn(data:any[]) {
  return request.delete('/gen/record/column', {data: getJsonWrapper({}, ['', data])})
}