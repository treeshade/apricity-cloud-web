import React, { useEffect, useState } from 'react';

import ApTable, { ApTableRef } from '@/components/Table';
import SearchSelect from '@/components/Fields/SearchSelect';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Form, Input, InputNumber, Modal, Popconfirm } from 'antd';
import { FormInstance } from 'antd/lib/form';
import { codeCategorySelectable, codeDelete, codeInsert, codeInstant } from './service';

const { TextArea } = Input;

const coreCodeTableRef = React.createRef<ApTableRef>();
const coreCodeInsertFormRef = React.createRef<FormInstance>();

const coreCodeColumns = [
  {
    title: '编码',
    dataIndex: 'code',
    key: 'code',
    editable: true,
  },
  {
    title: '编码名称',
    dataIndex: 'codeName',
    key: 'codeName',
    editable: true,
  },
  {
    title: '分类编码',
    dataIndex: 'codeCategoryName',
    key: 'codeCategoryName',
  },
  {
    title: '编码类型',
    dataIndex: 'codeClassName',
    key: 'codeClassName',
  },
  {
    title: '备注',
    dataIndex: 'remark',
    key: 'remark',
    editable: true,
  },
  {
    title: '排序码',
    dataIndex: 'orderNo',
    key: 'orderNo',
    editable: true,
  },
];

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

const SysCodePage: React.FC = () => {
  //hooks
  const [codeSelected, setCodeSelected] = useState({
    codeSelectedKeys: [] as any[],
    codeSelectedRows: [] as any[],
  });
  const [coreCodeInserModalVisible, setCoreCodeInserModalVisibleState] = useState(false);
  const [codeCategoryList, setCodeCategoryList] = useState([] as any[]);
  useEffect(() => {
    //查询select数据
    codeCategorySelectable().then((res) => {
      setCodeCategoryList(res);
    });
  }, []);
  useEffect(() => {
    //清空form
    if (coreCodeInserModalVisible) {
      coreCodeInsertFormRef.current?.resetFields();
    }
  }, [coreCodeInserModalVisible]);

  const handleCoreCodeCellChange = (field: any, oldVal: any, newVal: any) => {
    codeInstant('coreCodeServiceImpl', { id: oldVal.id, [field]: newVal[field] });
  };

  const handleCoreCodeFormChange = (
    event:
      | string
      | number
      | React.ChangeEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLTextAreaElement>
      | undefined,
    field: string,
  ) => {};

  const handleCoreCodeSelectChange = (selectedRowKeys: any[], selectedRows: any[]) => {
    setCodeSelected({ codeSelectedKeys: selectedRowKeys, codeSelectedRows: selectedRows });
  };

  const handleCoreCodeCurrentRowChange = (row: any) => {};

  const handleCoreCodeInsert = () => {
    setCoreCodeInserModalVisibleState(true);
  };

  const handleCoreCodeDelete = () => {
    codeDelete(codeSelected.codeSelectedRows).then((res) => {
      coreCodeTableRef.current?.refresh();
    });
  };

  return (
    <>
      <PageContainer>
        <ApTable
          ref={coreCodeTableRef}
          size="small"
          hightCurrentRow
          onCellChange={handleCoreCodeCellChange} //单元格值改变
          rowSelection={{
            type: 'checkbox',
            onChange: handleCoreCodeSelectChange,
          }}
          loadSuccess={(rows) => {
            if (rows && rows.length > 0) {
              coreCodeTableRef.current?.activeRowByIndex(0); //设置当前行
            }
          }}
          url={'/auth/sys/code/queries/raw'}
          currentRowChange={handleCoreCodeCurrentRowChange}
          columns={coreCodeColumns}
          toolbarRender={() => (
            <>
              <Button type={'primary'} onClick={handleCoreCodeInsert}>
                新建
              </Button>

              <Popconfirm
                title="您确认要删除吗?"
                okText="确定"
                cancelText="取消"
                onConfirm={handleCoreCodeDelete}
              >
                <Button type="primary" danger>
                  删除
                </Button>
              </Popconfirm>
            </>
          )}
        />

        <Modal
          title="新增"
          centered
          visible={coreCodeInserModalVisible}
          onOk={() => {
            const formData = coreCodeInsertFormRef.current?.getFieldsValue();
            codeInsert(formData).then((res) => {
              coreCodeTableRef.current?.refresh();
            });
            setCoreCodeInserModalVisibleState(false);
          }}
          onCancel={() => setCoreCodeInserModalVisibleState(false)}
        >
          <Form {...formItemLayout} initialValues={{ orderNo: 1 }} ref={coreCodeInsertFormRef}>
            <Form.Item
              label="编码"
              name="code"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreCodeFormChange(event, 'code')} />
            </Form.Item>
            <Form.Item
              label="编码名称"
              name="codeName"
              rules={[{ required: true, message: 'please input' }]}
            >
              <Input onChange={(event) => handleCoreCodeFormChange(event, 'codeName')} />
            </Form.Item>
            <Form.Item
              label="分类编码"
              name="codeCategoryId"
              rules={[{ required: true, message: 'please input' }]}
            >
              <SearchSelect
                text={"codeCategoryName"}
                val={"id"}
                filterable={true}
                items={codeCategoryList}
              />
            </Form.Item>
            <Form.Item label="备注" name="remark">
              <TextArea
                autoSize={{ minRows: 2, maxRows: 6 }}
                onChange={(event) => handleCoreCodeFormChange(event, 'remark')}
              />
            </Form.Item>
            <Form.Item label="排序码" name="orderNo">
              <InputNumber
                style={{ width: '100%' }}
                min={1}
                onChange={(event) => handleCoreCodeFormChange(event, 'orderNo')}
              />
            </Form.Item>
          </Form>
        </Modal>
      </PageContainer>
    </>
  );
};

export default SysCodePage;
