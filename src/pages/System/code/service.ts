import request from '@/utils/request';

export async function codeCategorySelectable(data?: any) {
  return request.get('/auth/sys/code/category/queries/selectable');
}

export async function codeInsert(data:any){
    return request.post('/auth/sys/code', {data: getJsonWrapper({}, ['', [{...data}]])})
}

export async function codeInstant(service:string, data:any) {
    return request.put('/auth/sys/code/instant', {data: getJsonWrapper({}, [service, [{...data}]])});
}

export async function codeDelete(data:any[]) {
  return request.delete('/auth/sys/code', {data: getJsonWrapper({}, ['', [...data]])});
}