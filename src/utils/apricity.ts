import { downloadRequest } from './request';
import request from './request';

export const getJsonWrapper = (p: ParamProps = {}, b: any[] = []): any => {
  return { p: p, b: b };
};

export const restPrint = (s: string, o: any): string => {
  return s.replace(/{[\w]+}/g, (match) => {
    return o[match.replace(/[{}]/g, '')];
  });
};

const Apricity = {
  post: async (url: string, params?: any, options?: any): Promise<any> => {
    return request.post(url, { data: { ...params }, ...options });
  },
  get: async (url: string, params?: any, options?: any): Promise<any> => {
    return request.get(url, { data: { ...params }, ...options });
  },
  param: (o: any, questionMark?: boolean): string => {
    const prev = questionMark === false ? '' : '?';
    let serialize = '';
    for (const k in o) {
      if (Object.prototype.hasOwnProperty.call(o, k)) {
        const v = o[k];
        if (v) {
          serialize += '&' + (k + '=' + v);
        }
      }
    }
    return prev + serialize.replace('&', '');
  },
  download: (url: string, params?: any, options?: any) => {
    downloadRequest.get(url, { data: { ...params }, ...options }).then((res) => {
      const blob = new Blob([res.data]);
      const url = window.URL.createObjectURL(blob);
      // 以动态创建a标签进行下载
      const a = document.createElement('a');
      //将返回的文件名作为下载的文件名
      let fileName = res.response.headers
        .get('content-disposition')
        ?.split(';')[1]
        .split('=')[1]
        .split('"')[1];
      fileName = fileName ? fileName : 'download';
      a.href = url;
      a.download = fileName;
      a.click();
      window.URL.revokeObjectURL(url);
    });
  },
  resolveExt$: (data: any[]): any[] => {
    const newData = [] as any[];
    if (data && data.length > 0) {
      for (let i = 0; i < data.length; i++) {
        const e = { ...data[i], ...data[i]['ext$'] };
        delete e['ext$'];
        newData.push(e);
      }
    }
    return newData;
  },
};

export default Apricity;
