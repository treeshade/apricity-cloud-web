export type TokenType = {
    additionalInformation?: {
        jti?: string
    },
    expiration?: number,
    expired?: boolean,
    expiresIn?: number,
    refreshToken?: {
        expiration?: number,
        value?: string
    },
    scope?: string[],
    tokenType?: string,
    value?: string
}

const APRICITY_CLOUD_TOKEN_NAME = "apricity_cloud_token"

export function setToken(token:TokenType): void {
    localStorage.setItem(APRICITY_CLOUD_TOKEN_NAME, JSON.stringify(token))
}

export function getToken(): TokenType{
    return JSON.parse(localStorage.getItem(APRICITY_CLOUD_TOKEN_NAME) as string) as TokenType
}

export function getAccessToken():string{
    return getToken?.()?.value as string
}

export function getExpiration():number{
    return getToken?.()?.expiration as number
}

export function getRefreshToken():string {
    return getToken?.()?.refreshToken?.value as string
}

export function getRefreshTokenExpiration():number{
    return getToken?.()?.refreshToken?.expiration as number
}

export function getTokenType():string{
    return getToken?.()?.tokenType as string
}
