
/**
 * 获取当前路由的路径
 * @param {*} routes 
 * @param {*} location 
 * @param {*} historyPath 
 */
export const findHistoryPath = (routes, location, historyPath) => {
    if(routes){
        for (let i = 0; i < routes.length; i++) {
            const route = routes[i];
            if(route.path === location.pathname){
                historyPath.push(route)
                return true
            }else{
                if(findHistoryPath(route.routes, location, historyPath) && route.path !== '/'){
                    historyPath.push(route)
                }
            }
        }
    }
    return false
}