export interface SelectConfigsProps {
    category: string;
    url?: string;
    items?: any[];
    text: string;
    val: string;
}

const selectConfigs: SelectConfigsProps[] = [
    {
        category: 'code',
        url: '/auth/sys/code/queries/selectable',
        text: 'codeName',
        val: 'code'
    },
]

selectConfigs.forEach(config => {
    config.text?  config.text :  config.text = 'label';
    config.val?  config.val :  config.val = 'value';
})

export default selectConfigs