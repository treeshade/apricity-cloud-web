/**
 * search，选择框不可输入
 */

import { Input, Select } from 'antd';
import React, { useState } from 'react';

const { Option } = Select;

export interface SearchSelectProps {
  items?: any[];
  text: string;
  val: string;
  filterable?: boolean; //开启筛选
  searchPlaceholder?: string;
  remoteMethod?: (event: any) => void; //搜索改变
  value?: string;//满足自定义表单组件的条件
  onChange?:(value:string) => void;//满足自定义表单组件的条件
}

const SearchSelect: React.FC<SearchSelectProps> = ({
  items,
  searchPlaceholder,
  remoteMethod,
  filterable,
  text,
  val,
  value,
  onChange
}) => {
  const [options, setOptions] = useState([...(items as any[])]);

  const handleSeachChange = (event: any) => {
    remoteMethod?.(event);
    if (filterable && options) {
      if (event.target.value) {
        setOptions(
          items?.filter((option) => option?.[text].indexOf(event.target.value) !== -1) as any[],
        );
      } else {
        setOptions([...(items as any[])]);
      }
    }
  };


  const handleSelect = (v: any) => {
    onChange?.(v)
  }

  return (
    <>
      <Select
       onSelect={ handleSelect }
        dropdownRender={(menu) => {
          return (
            <div>
              {filterable ? (
                <div>
                  <Input placeholder={searchPlaceholder} onChange={handleSeachChange} />
                </div>
              ) : (
                <></>
              )}
              {menu}
            </div>
          );
        }}
        
      >
        {options?.map((option) => (
          <Option key={option[val]} value={option[val]}>
            {option[text]}
          </Option>
        ))}
      </Select>
    </>
  );
};

SearchSelect.defaultProps = {
  searchPlaceholder: '请搜索',
  filterable: false,
};

export default SearchSelect;
