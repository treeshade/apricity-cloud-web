import Apricity from "@/utils/apricity"
import selectConfigs from '@/config/select-config'

export const refreshSelectItem = (col:any) => {
    if(col?.category){
      const config = selectConfigs.find(config => config.category === col.category )
      if(config && config.category && config.url){
        const url = col.param ? config.url  +  Apricity.param(col.param) : config.url
        Apricity.get(url).then(res => {
          if(res){
            col.items = (res as any[]).map(item => {return {text: item[config.text], val: item[config.val]}})
          }
        })
      }
    }
  }