import { Input, Select } from 'antd';

const { Option } = Select;

export const renderField = (
  fieldType: string | undefined,
  save?: (val: any, cellText?:string) => void,
  close?: () => void,
  props?: any,
  form?:any,
  instance?:
    | string
    | ((instance: Input | null) => void)
    | React.RefObject<Input>
    | React.MutableRefObject<undefined>
    | null
    | undefined,
): React.ReactNode => {
  if (fieldType) {
    switch (fieldType) {
      case 'select':
        return (
          <Select
            ref={instance}
            autoFocus={true}
            labelInValue = {true}
            onSelect={(item) => {
              close?.();
              save?.(item.value, item.label);
            }}
            onBlur={() => close?.()}
          >
            {renderOptions(props?.items)}
          </Select>
        );
      default:
        break;
    }
  }
  return (
    <Input
      ref={instance}
      onPressEnter={(e) => {
        close?.();
        save?.((e.target as any).value);
      }}
      onBlur={(e) => {
        close?.();
        save?.((e.target as any).value);
      }}
    />
  );
};

const renderOptions = (items: any[]): React.ReactNode => {
  return items ? items.map((item) => {
    return (
      <Option key={item.val} value={item.val}>
        {item.text}
      </Option>
    );
  }) : <></>
};
