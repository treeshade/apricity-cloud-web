import { Form, Input, Select } from 'antd';
import { FormInstance, FormProps } from 'antd/lib/form';
import React, { useImperativeHandle } from 'react';
import { forwardRef } from 'react';
import { renderField } from '../Fields';
import { refreshSelectItem } from '../Fields/utils';

const formRef = React.createRef<FormInstance>();

export interface ApFormRef {
  getFormInstance: () => React.RefObject<FormInstance<any>>;
}

interface ApFormProps {
  fields?: any[];
}

const ApForm = forwardRef<ApFormRef, ApFormProps & FormProps<any>>(({ fields }, ref) => {
  useImperativeHandle(ref, () => ({
    getFormInstance,
  }));

  //刷新select数据
  fields?.forEach((field) => {
    if (field.fieldType) {
      switch (field.fieldType) {
        case 'select':
          //刷入下拉框数据
          refreshSelectItem(field);
          break;
        default:
          break;
      }
    }
  });

  const getFormInstance = () => {
    return formRef;
  };

  return (
    <Form ref={formRef}>
      {fields?.map((field) => {
        return renderField(field.type);
      })}
      {fields?.map((field) => {
        switch (field.type) {
          case 'select':
            return <Select></Select>;
          default:
            return <Input />;
        }
      })}
    </Form>
  );
});
