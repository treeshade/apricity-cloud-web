import { Table, Row, Col } from 'antd';
import React, { forwardRef, ReactNode, useEffect, useImperativeHandle, useState } from 'react';
import request from '@/utils/request';
import { SizeType } from 'antd/lib/config-provider/SizeContext';
import { EditableRow, EditableCell } from './Editable';
import { TablePaginationConfig, TableProps } from 'antd/lib/table';
import Apricity from '@/utils/apricity';
import { refreshSelectItem } from '../Fields/utils'

import selectConfigs from '@/config/select-config'

const defaultPaginationProps = {
  pageSizeOptions: ['10', '20', '50'],
  current: 1,
  pageSize: 20,
  showSizeChanger: true,
  showQuickJumper: true,
  showTotal: (total, range) => `共 ${total} `,
} as TablePaginationConfig;

export interface ApTableProps {
  url?: string;
  size?: SizeType;
  service?: string;
  columns?: any[];
  requestData?: any;
  dataSource?: any[];
  hightCurrentRow?: boolean; //高亮当前行
  pagination?: false | TablePaginationConfig;
  toolbarRender?: () => ReactNode; //工具栏
  currentRowChange?: (row: any) => any; //当前行变化
  onCellChange?: (field: string, oldVal: any, newVal: any) => any;
  loadSuccess?: (rows: any[]) => void; //数据加载成功
}

export interface RawResponse {
  columnTypeMap?: any; //列类型map
  current?: number; //当前页
  pages?: number; //页码总数
  records?: any[]; //数据
  size?: number; //每页记录数
  total?: number; //总记录数
}

export interface ApTableRef {
  refresh: (url?: string, requestData?: any) => void;
  activeRowByIndex: (index: number) => void;
  getCurrebtRow: () => any;
}

const ApTable = forwardRef<ApTableRef, ApTableProps & TableProps<any>>(
  (
    {
      pagination,
      url,
      requestData,
      currentRowChange,
      hightCurrentRow,
      dataSource,
      columns,
      onCellChange,
      toolbarRender,
      loadSuccess,
      ...restProps
    },
    ref,
  ) => {
    useImperativeHandle(ref, () => ({
      refresh,
      activeRowByIndex,
      getCurrebtRow
    }));

    const getCurrebtRow = ():any => {
      return currentRowState
    }
    
    /* =======  hooks  ====== */
    const [currentRowState, setCurrentRowState] = useState({} as any);
    const [rows, setRows] = useState([] as any[]);
    const [page, setPage] = useState(pagination ? pagination : false);

    useEffect(() => {
      if (url) {
        refresh(url, requestData);
      }
      //刷新select数据
      columns?.forEach(col => {
        if(col.fieldType){
          switch (col.fieldType) {
            case 'select':
              //刷入下拉框数据
              refreshSelectItem(col)
              break;
          
            default:
              break;
          }
        }
      });
    }, [url, requestData]);


    /* ===========  function  ============ */
    //单元格点击
    const handleCellClick = (row: any, event: React.MouseEvent<HTMLElement, MouseEvent>) => {
      if (!currentRowState || currentRowState.index !== row.index) {
        setCurrentRowState({ ...row });
        currentRowChange && hightCurrentRow ? currentRowChange(row) : '';
      }
    };

    const rowClassName = (row: any, index: any) => {
      row.index = index;
      return currentRowState.id === row.id && hightCurrentRow ? `ant-table-row-active` : ``;
    };

    //刷新表格
    const refresh = (requestUrl: string = url as string, params: any = requestData) => {

      const pageParam = { n: 1, s: 20 };
      if (page) {
        const realPage = page as TablePaginationConfig;
        pageParam.n = realPage.current ? realPage.current : 1;
        pageParam.s = realPage.pageSize ? realPage.pageSize : 20;
      } else {
        pageParam.s = -1;
      }
      requestUrl
        ? request
            .post(requestUrl, {
              data: getJsonWrapper({ f: { ...params }, ...pageParam }, []),
            })
            .then((res) => {
              setRows(Apricity.resolveExt$(dataSource ? dataSource : res ? [...res.records] : []));
              setPage({ current: res.current, total: res.total, pageSize: res.size });
              loadSuccess?.(res.records);
            })
        : '';
    };

    //激活行
    const activeRowByIndex = (index?: number) => {
      if (index === 0 || index) {
        const currentRow = rows.find((row) => row.index === index);
        setCurrentRowState(currentRow);
        currentRowChange && hightCurrentRow ? currentRowChange(currentRow) : '';
      }
    };

    //列转换
    const mapColumns = columns?.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: (record: any) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          items: col.items,
          fieldType: col.fieldType,
          selectId: col.selectId,
          handleSave: handleSave,
        }),
      };
    });

    const handleSave = (field: string, oldVal: any, newVal: any) => {
      onCellChange?.(field, oldVal, newVal);
    };

    return (
      <>
        <Row gutter={16} className={'ap-table-toolbar-wrap'}>
          <Col>{toolbarRender?.()}</Col>
          <Col></Col>
        </Row>
        <Table
          rowClassName={rowClassName}
          pagination={pagination}
          onRow={(record) => {
            return {
              onClick: (event) => {
                handleCellClick(record, event);
              },
            };
          }}
          components={{
            body: {
              row: EditableRow,
              cell: EditableCell,
            },
          }}
          {...restProps}
          columns={mapColumns}
          rowKey={(row) => row.id}
          dataSource={rows}
        />
      </>
    );
  },
);

ApTable.defaultProps = {
  pagination: {
    ...defaultPaginationProps,
  },
};

export default ApTable;
