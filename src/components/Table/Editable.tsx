import React, { useContext, useEffect, useRef, useState } from 'react';
import { Form } from 'antd';
import { renderField } from '@/components/Fields';

const EditableContext = React.createContext<any>({} as any);

interface EditableRowProps {
  index: number;
}

export const EditableRow: React.FC<EditableRowProps> = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface EditableCellProps {
  title: React.ReactNode;
  editable: boolean;
  children: React.ReactNode;
  dataIndex: string;
  record: any;
  items: any[];
  fieldType?: string;
  selectId?: string;
  handleSave: (dataIndex: string, oldVal: any, newVal: any) => void;
}

export const EditableCell: React.FC<EditableCellProps> = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  fieldType,
  selectId,
  ...restProps
}) => {
  const row = { ...record };
  if (record && record['ext$']) {
    const ext$ = { ...record['ext$'] };
    for (const key in ext$) {
      if (Object.prototype.hasOwnProperty.call(ext$, key)) {
        row[key] = ext$[key];
      }
    }
  }

  let initCellVal = ''

  if(fieldType){
    switch (fieldType) {
      case 'select':
        if(restProps.items){
          const valItem = restProps.items.find(item => item.val === row[dataIndex])
          if(valItem){
            initCellVal = valItem.text
          }
        }
        break;
      default:
        initCellVal = dataIndex ? row[dataIndex] : ''
        break;
    }
  }else{
    initCellVal = dataIndex ? row[dataIndex] : ''
  }

  const [editing, setEditing] = useState(false);
  const [cellValue, setCellValue] = useState(initCellVal);
  const [formItemVal, setFormItemVal] = useState(dataIndex ? row[dataIndex] : '');

  const instance = useRef();
  const form = useContext(EditableContext);

  useEffect(() => {
    if (editing) {
      (instance.current as any)?.focus?.();
    }
  }, [editing]);

  const toggleEdit = () => {
    if(formItemVal){
      form.setFieldsValue({ [dataIndex]: formItemVal });
    }
    if (!editing) {
      setEditing(!editing);
    }
  };

  const save = async (val: any, cellText: any) => {
    try {
      let values;
      switch (fieldType) {
        case 'select': //select 保存
          values = await form.validateFields();
          if (values[dataIndex] !== val) {
            handleSave(dataIndex, row, { [dataIndex]: val });
          }
          setCellValue(cellText);
          setFormItemVal(val);
          break;
        default:
          values = await form.validateFields();
          if (cellValue !== values[dataIndex]) {
            handleSave(dataIndex, row, { [dataIndex]: values[dataIndex] });
          }
          setCellValue(val)
          setFormItemVal(val)
          break;
      }
      toggleEdit();
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  const close = () => {
    if (editing) {
      setEditing(!editing);
    }
  };

  const renderEitableChildNode = () => {
    return editing ? (
      <Form.Item
        style={{ margin: 0 }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `请填写${title}`,
          },
        ]}
      >
        {renderField(fieldType, save, close, { ...restProps }, form, instance )}
      </Form.Item>
    ) : (
      <div className="editable-cell-value-wrap">{cellValue}</div>
    );
  };

  if (editable) {
    return <td onClick={toggleEdit}>{renderEitableChildNode()}</td>;
  }

  return <td {...restProps}>{children}</td>;
};
