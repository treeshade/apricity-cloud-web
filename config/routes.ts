﻿
export interface RouteProps {
  name?:string;
  path: string;
  component: string;
  routes?: RouteProps[];
  authority?:string[];
  icon?:string,
  hideInMenu?: boolean
}

export default [
  {
    path: '/',
    component: '../layouts/BlankLayout',
    routes: [
      {
        path: '/user',
        component: '../layouts/UserLayout',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './User/login',
          },
        ],
      },
      {
        path: '/',
        component: '../layouts/SecurityLayout',
        routes: [
          {
            path: '/',
            component: '../layouts/BasicLayout',
            authority: ['admin', 'user'],
            routes: [
              {
                path: '/',
                redirect: '/welcome',
              },
              {
                path: '/welcome',
                name: 'welcome',
                icon: 'smile',
                component: './Welcome',
              },
              {
                path: '/admin',
                name: 'admin',
                icon: 'crown',
                component: './Admin',
                authority: ['admin'],
                routes: [
                  {
                    path: '/admin/sub-page',
                    name: 'sub-page',
                    icon: 'smile',
                    component: './Welcome',
                    authority: ['admin'],
                  },
                ],
              },
              {
                name: 'sys',
                icon: 'ToolOutlined',
                path: '/sys',
                routes: [{
                  name: 'code',
                  path: '/sys/code',
                  component: './System/code',
                }]
              },
              {
                name: 'userOrg',
                icon: 'ToolOutlined',
                path: '/user-org',
                routes: [{
                  name: 'user',
                  path: '/user-org/user',
                  component: './UserOrg/user',
                },{
                  name: 'org',
                  path: '/user-org/org',
                  component: './UserOrg/org',
                },{
                  name: 'role',
                  path: '/user-org/role',
                  component: './UserOrg/role',
                }]
              },
              {
                name: 'gen',
                icon: 'CodeOutlined',
                path: '/gen',
                routes: [
                  {
                    path: '/',
                    redirect: '/gen/list',
                  },
                  {
                    name: 'list',
                    icon: 'CodeOutlined',
                    path: '/gen/list',
                    component: './Gen',
                  }
                ]
              },
              {
                name: 'demo',
                icon: 'CodeOutlined',
                path: '/demo',
                component: './Demo',
              },
              {
                component: './404',
              },
            ],
          },
          {
            component: './404',
          },
        ],
      },
    ],
  },
  {
    component: './404',
  },
] as RouteProps[];
